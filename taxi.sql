"1) создание базы данных и таблиц" 

CREATE DATABASE kurut_taxi;
CREATE TABLE cars (id bigint PRIMARY KEY, brand varchar(255), model varchar(255), fuel varchar(100), engine_displacement float, type_of_gearbox varchar(100), year integer, color varchar(100));
CREATE TABLE drivers (id bigint PRIMARY KEY, first_name varchar(255), last_name varchar(255), date_of_birth integer, driving_experience integer, gender varchar, car_id bigint REFERENCES cars(id));
CREATE TABLE call_center_operators (id bigint PRIMARY KEY, first_name varchar(255), last_name varchar(255), date_of_birth integer, gender varchar);
"____________________________________________________________________________________________________"
"2) Заполнение таблиц данными"

'cars'
INSERT INTO cars VALUES (11, 'BMW', 'X5', 'Бензин', 3.2, 'Автомат', 2005, 'Черный');
INSERT INTO cars VALUES (12, 'BMW', 'X5', 'Бензин', 3.2, 'Механика', 2010, 'Белый');
INSERT INTO cars VALUES (13, 'Toyota', 'Camry', 'Бензин', 3.0, 'Механика', 2004, 'Серый');
INSERT INTO cars VALUES (14, 'Toyota', 'Camry', 'Бензин', 3.1, 'Механика', 2007, 'Белый');
INSERT INTO cars VALUES (15, 'Toyota', 'Camry', 'Бензин', 2.0, 'Автомат', 2010, 'Черный');
INSERT INTO cars VALUES (16, 'Mercedes', 'S200', 'Бензин', 3.0, 'Автомат', 2013, 'Черный');
INSERT INTO cars VALUES (17, 'Mercedes', 'E200', 'Бензин', 3.0, 'Автомат', 2010, 'Белый');
INSERT INTO cars VALUES (18, 'Mercedes', 'ER16', 'Бензин', 3.2, 'Механика', 2011, 'Темно-синий');
INSERT INTO cars VALUES (19, 'Tesla', 'S', 'Электричество', 450, 'Автомат', 2015, 'Серебристый');
INSERT INTO cars VALUES (20, 'Tesla', 'E', 'Электричество', 500, 'Автомат', 2016, 'Черный');
INSERT INTO cars VALUES (21, 'Tesla', 'X', 'Электричество', 600, 'Автомат', 2017, 'Белый');

'drivers'
INSERT INTO drivers Values (1, 'Erlan', 'Turdubaev', 2000, 2, 'male', 13);
INSERT INTO drivers Values (2, 'Edil', 'Sydykov', 2001, 1, 'male', 11);
INSERT INTO drivers Values (3, 'Medina', 'Ellebesova', 1999, 2, 'male', 12);
INSERT INTO drivers Values (4, 'Akmaral', 'Akimova', 1999, 2, 'female', 16);
INSERT INTO drivers Values (5, 'Baisal', 'Janybekov', 1983, 12, 'male', 15);
INSERT INTO drivers Values (6, 'Elnura', 'Asanova', 1985, 10, 'female', 14);
INSERT INTO drivers Values (7, 'Meerim', 'Bakrybekova', 1989, 11, 'female', 18);
INSERT INTO drivers Values (8, 'Abdykalyk', 'Toksonov', 1999, 3, 'male', 17);
INSERT INTO drivers Values (9, 'Temirlan', 'Uranov', 1962, 25, 'male', 19);
INSERT INTO drivers Values (10, 'Asel', 'Adylova', 1975, 20, 'female', 20);
INSERT INTO drivers Values (11, 'Azamat', 'Azamatov', 2001, 1, 'male', 13);

'call_center_operators'
INSERT INTO call_center_operators VALUES(22, 'Erulan', 'Sadykov', 2000, 'male');
INSERT INTO call_center_operators VALUES(23, 'Dilnoz', 'Sadykova', 2000, 'female');
INSERT INTO call_center_operators VALUES(24, 'Astra', 'Alieva', 1986, 'female');
INSERT INTO call_center_operators VALUES(25, 'Kylymjan', 'Maametkadyrova', 1997, 'female');
INSERT INTO call_center_operators VALUES(26, 'Kylym', 'Aibekov', 1999, 'male');
INSERT INTO call_center_operators VALUES(27, 'Zarina', 'Sultangazieva', 2000, 'female');
INSERT INTO call_center_operators VALUES(28, 'Nurzada', 'Salamova', 1973, 'female');
INSERT INTO call_center_operators VALUES(29, 'Edil', 'Baisalov', 1988, 'male');
INSERT INTO call_center_operators VALUES(30, 'Nargiza', 'Aizhigitova', 1999, 'female');
INSERT INTO call_center_operators VALUES(31, 'Aizirek', 'Akylova', 1998, 'female');
INSERT INTO call_center_operators VALUES(32, 'Erulan', 'Baibekov', 1983, 'male');
INSERT INTO call_center_operators VALUES(33, 'Erulan', 'Edilov', 1995, 'male');
'_______________________________________________________'
'3)' SELECT * FROM cars WHERE model = 'Camry' ORDER BY year DESC;

'4)' SELECT DISTINCT first_name FROM call_center_operators ORDER BY first_name DESC LIMIT 10;

'5)' UPDATE cars SET brand = 'Mersus' WHERE brand = 'Mercedes';

'6)' DELETE FROM drivers WHERE first_name = 'Azamat' and last_name = 'Azamatov';

'7)' SELECT * FROM drivers WHERE driving_experience > 10 and gender = 'female';

'8)' SELECT AVG(date_of_birth) FROM drivers WHERE date_of_birth > '1975';

'9)' 

'10)' SELECT brand, COUNT(*) AS number_of_cars FROM cars GROUP BY brand HAVING COUNT(*) > 0 ORDER BY number_of_cars DESC;

'11)' SELECT  car_id, COUNT(*) AS number_of_cars FROM drivers GROUP BY car_id HAVING COUNT(*) > 0 ORDER BY number_of_cars DESC;